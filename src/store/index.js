export default {
    debug: true,
    state: {
        currentView: "home"
    },
    setCurrentView(newValue) {
        this.state.currentView = newValue
    },
}