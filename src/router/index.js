import Vue from "vue";
import VueRouter from "vue-router";
import StartScreen from "../views/StartScreen.vue";
import TriviaPage from "../views/TriviaPage.vue";

Vue.use(VueRouter);

const routes = [
    {
        path: "/",
        name: "StartScreen",
        component: StartScreen,
    },
    {
        path: "/game",
        name: "TriviaPage",
        component: TriviaPage,
    },
];

const router = new VueRouter({
    routes,
});

export default router;
