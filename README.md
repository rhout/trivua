# Trivua the fun and giggles trivia game

A web app where you get 10 random questions about everything in all difficulty levels.

## Getting started from terminal:

Clone the repository in the desired folder with

```
git clone https://gitlab.com/rhout/trivua
```

Install dependencies:

```
cd trivua
npm i
```

Run the application:

```
npm run serve
```

## Getting started with a demo
Check [this](https://trivua.herokuapp.com/) out to see a demo of the game.

Happy guessing!
